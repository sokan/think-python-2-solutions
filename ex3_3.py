#!/usr/bin/env python3

print('*' * 10, '1st part below', '*' * 10)

def do_four(value):
    print(value)
    print(value)
    print(value)
    print(value)

def grid_2x2():
    lines = ('+ ' + '- ' * 4) * 2 + '+'
    columns = ('|' + ' ' * 9) * 2 + '|'
    print(lines)
    do_four(columns)
    print(lines)
    do_four(columns)
    print(lines)

grid_2x2()

print()
print('*' * 10, '2nd part below', '*' * 10)
print()

#######2nd part of excersise#######


line = ('+ ' + '- ' * 4)
column = ('|' + ' ' * 9)

#print(line * 2)
def do_twice(f):
    f()
    f()

def do_four(f):
    do_twice(f)
    do_twice(f)

def grid_rows():
    do_four(print(line))

def grid_columns():
    print(column)

#def half_grid():
    

#def grid_4x4():
#    do_twice(half_grid)

#print(line)
grid_rows()
#grid_4x4()
